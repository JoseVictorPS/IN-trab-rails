Rails.application.routes.draw do

  root 'static_pages#home'

  resources :users, except: [:new], path:'usuarios'
  #resources :posts
  resources :searches

  get '/cadastro', to: 'users#new'

  get '/login', to: "sessions#new"
  post '/login', to: "sessions#create"
  delete '/logout', to: "sessions#destroy"

  #post '/user_id/:id', to: "user#posting"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
